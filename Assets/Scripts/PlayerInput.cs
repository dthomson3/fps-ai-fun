﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {
    bool hideBody;
    bool isHiding;
    public GameObject sniperCrossHair;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            print("SCOPE");
            GetComponentInChildren<Animator>().SetBool("scopingIn", true);

        }
        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            print("UNSCOPE");
            GetComponentInChildren<Animator>().SetBool("scopingIn",false);
        }
        
        if (GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).IsName("sniperZoom"))
        {
            GetComponentInChildren<MeshRenderer>().enabled = false;
            sniperCrossHair.gameObject.SetActive(true);
        }
        else
        {
            GetComponentInChildren<MeshRenderer>().enabled = true;
            sniperCrossHair.gameObject.SetActive(false);
        }
    }
}

